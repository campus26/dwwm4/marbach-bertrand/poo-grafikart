<?php
//renamed to be correctly listed in explorer
require '03-04character.php';

// echo 'hello lo';

$merlin= new character("Merlin", "Myrddin");
$merlin->regenerate(5);

$potter= new character ("Potter", "Gavin");

// $potter->regenerate();
// $potter->life=-20;
// echo '<pre>';
// var_dump ($potter->killed());
// echo '</pre>';
echo "<br> Début de l'histoire";
echo '<pre>';
echo $merlin->getFirstName() . "<br>"; //Get: way of transferring a private property
var_dump ($merlin);
var_dump ($potter);
echo '</pre>';

$merlin->attacks($potter);

if ($potter->killed()){
    echo '<br> Oooooh, le pauvre Gavin est mort<br>';
    } else {
        //look out for the way of getting the getter echoed. $potter->getLife() instead of $potter->life if it was public
        echo '<br> Il a survécu et sort renforcé (tout ce qui ne tue pas renforce !...)<br>Il lui reste : ' . $potter->getLife() . ' fors<br>';
    }

echo "<br> Bilan du tour de jeu";
echo '<pre>';
var_dump ($merlin);
var_dump ($potter);
echo '</pre>';