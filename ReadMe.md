```
**Leçons grafikart** en reférence : https://grafikart.fr/formations/programmation-objet-php

Dans VisualStudio, quand on veut run le projet php : php -S localhost:0418  

```

```
**Leçon 3  Objets : classes, propriétés, méthodes**
https://youtu.be/xmoOvoiPNhU
   - propriété : variables spécifiques ou caractéristiques
   - méthodes : peuvent, comme des fonctions, manipuler l'instance
   - instances : objets de même classe. Les instances partagent la même structure définie par la classe, mais elles peuvent avoir des états différents, car chaque instance peut avoir ses propres valeurs pour les attributs de la classe
```

```
**Leçon 4  Visibilité**
https://youtu.be/zZ_tVAPfGAA
Il y a trois niveaux de visibilité :
   - public : propriété ou méthode accessible au sein de la classe ($this) et en dehors
   - private : propriété ou méthode accessible à l'intérieur de la classe, mais pas en dehors
   - protected : propriété ou méthode accessible à l'intérieur de la classe et des classes héritées

   Par défaut on choisira le 'private' et on mettra en place des 
         'getters' 
    et des 
         'setters'
/getter/$potter->getLife() vs /public/$potter->life
/setter/public function setNom($nom){$this->nom = $nom} dans les méthodes dans la classe


==> Précisions :
 [] Si une propriété est déclarée en utilisant le mot clef var, elle sera définie comme publique
 [] Si une méthode est déclarée sans visibilité, elle sera définie comme publique

===> Attention
public function __construct($name){$this->name=$name;} la méthode 'construct' est une méthode particulière utilisée seulement pour initialiser les objets quand ils sont créés et il ne peut y avoir qu'un contructeur par classe.

```

```
**Leçon 5  Formulaire**
https://youtu.be/rTGmcdFAWqw

```

```
**Leçon 6 :  Bien documenter ses classes**
https://youtu.be/KAL1vJtHces
Dans un dev en php, il est important de documenter ses classes. Ceci en se rappelant qu'il s'agit
d'un langage non typé 
```

```
**Leçon 7 : Propriétés et Méthodes statiques **
https://youtu.be/8T8UqCOpXuM
    static    |    self    |    const
Les propriétés et les méthodes statiques peuvent être utilisées sans avoir besoin d'instancier
la classe, On peut y accéder directement en utilisant le nom de la class
-> L'emploi du terme 'static' indique que la classe elle même va avoir la méthode décrite.
-> Se souvenir de l'instruction 'self::' à insérer dans la class elle-même.
-> exemples d'imbrication d'une fonction publique appelant une fonction privée avec le même paramètre.
-> démonstration de la définition et de l'utilisation d'une consante
```

```
**Leçon 8 :  **