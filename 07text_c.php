<?php

class TextTrois
{
    
    const SUFFIX = " €";
    
    public static function DefinitivelyWithOrWithoutZero($number)
    {
        if ($number < 10) {
            return '0' . $number.self::SUFFIX;
        } else {
            return $number.self::SUFFIX;
        }
    }
}