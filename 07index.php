<?php

require'07text_a.php';
require'07text_b.php';
require'07text_c.php';

require'07character_b.php';

//use of static method, here Text::
echo "Utilisation de la propriété 'static' dans les class d'objets<br>";
echo '  =-> faire varier le chiffre dans le code<br>';
echo '<pre>';

var_dump (TextUn::PublicWithZero(10));

echo '</pre>';
echo 'fin de la première démo.<br><br><br><br>';



//use of suffix method
echo "Utilisation de la propriété 'suffix' dans les class d'objets<br>";
echo '  =-> faire varier le suffixe dans le code<br>';
echo '<pre>';

var_dump(TextDeux::WithOrWithoutZero(3));

echo '</pre>';
echo 'fin de la deuxième démo.<br><br><br><br>';



//use of const
echo "Utilisation d'une constante dans les class d'objets<br>";
echo '  =-> faire varier la constante (!!!) dans le code<br>';
echo '<pre>';

var_dump(TextTrois::DefinitivelyWithOrWithoutZero(3));

echo '</pre>';
echo 'fin de la troisième démo.<br><br><br><br>';


//use of const in Character (07character_b.php)
echo "Utilisation d'une constante dans les class d'objets<br>";
echo '  =-> faire varier la constante (!!!) dans le code<br>';
echo '<pre>';

$merlin = new character_b('Merlin_b','Fintan');
$merlin->regenerate();
var_dump($merlin);

echo '</pre>';
echo 'fin de la quatrième démo.<br><br><br><br>';