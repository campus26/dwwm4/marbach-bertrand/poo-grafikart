<?php
//renamed to be correctly listed in explorer

/**
 * Class Form
 * Quick and simple generation of a form
 */
class Form{

    /**
     * @var string tag used to surround fields
     */
    public $surround = 'p'; //to be used in the private function, but the public declaration will allow to call the method from elsewhere as well

    /**
     * @var array form used data
     */
    private $data; //to later be able to store those data

    //set up the construtor
    /**
     * @param array data
     * @param string $string
     */
    public function __construct($data=array()){ //by default passing value is an empty table
        $this->data=$data;
    }

    /**
     * @param $html string html code to be surronded
     * @return string
     */
    private function surround($html){
        return "<$this->surround>$html</$this->surround>";
    }

    /**
     * @param $index string index of the value to be retreived
     * @return string
     */
    private function getValue($index) {
        return isset($this->data[$index]) ? $this->data[$index] : null;
    }

    /**
     * @param $label string field label to be displayed
     * @param $type string describes field type
     * @param $name string
     * @return string
     */
    public function input ($label, $type, $name){
        return $this->surround(
            '<p><label for="'.$name.'" />'.$label.'</label><input type="'.$type.'" id="" name="' .$name. '" id="'.$name.'" value="'.$this->getValue($name).'" /></p>'
        );
    }

    /**
     * @return string
     */
    public function submit(){
        return $this->surround('<button type ="submit">Envoyer</button>');
    }
}
