<?php

//'static' property demo
class TextUn
{
    public static function PublicWithZero($number)
    {
        return self::withZero($number);
    }
    
    private static function withZero($number)
    {
        if ($number < 10) {
            return '0' . $number;
        } else {
            return $number;
        }
    }
}