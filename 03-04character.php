<?php
//renamed to be correctly listed in explorer
class character
{
    private $firstname; 
    private $name;
    private $life = 20;
    private $atk = 30;

    public function __construct($name, $firstname)
    {
        $this->name = $name;
        $this->firstname = $firstname;
    }

    //Way of transferring a private property : Getters
    //No getters are needed in case of public properities
    public function getFirstName()
    {
        return $this->firstname;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLife()
    {
        return $this->life;
    }

    public function getAtk()
    {
        return $this->atk;
    }

    public function yell()
    {
        echo 'LEEEEEROY JENKINS';
    }

    public function regenerate($life = null)
    {
        if (is_null($life)) {
            $this->life = 100;
        } else {
            $this->life += $life;
        }
    }

    public function killed()
    {
        return $this->life <= 0;
    }

    //Example of a private function
    private function no_negative_life()
    {
        if ($this->life < 0) {
            echo '<br>Fors' . $this->life . '<br>';
            echo $this->name . '<br>';
            $this->life = 0;
        }
    }

    public function attacks($target)
    {
        //attacker is $this
        //defender is $target
        $target->life -= $this->atk;

        echo '<pre>';
        echo "Dans l'attaque <br>";
        var_dump($target);
        echo '</pre>';

        //use case showing pertinent use of private function
        $target->no_negative_life();
    }
}