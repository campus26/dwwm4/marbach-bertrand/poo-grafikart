<?php
//renamed to be correctly listed in explorer

class Form{

    public $surround = 'p'; //to be used in the private function, but the public declaration will allow to call the method from elsewhere as well

    private $data; //to later be able to store those data

    //set up the construtor
    public function __construct($data=array()){ //by default passing value is an empty table
        $this->data=$data;
    }

    private function surround($html){
        return "<$this->surround>$html</$this->surround>";
    }

    private function getValue($index) {
        return isset($this->data[$index]) ? $this->data[$index] : null;
    }

    public function input ($label, $type, $name){
        return $this->surround(
            '<p><label for="'.$name.'" />'.$label.'</label></p><p><input type="'.$type.'" id="" name="' .$name. '" id="'.$name.'" value="'.$this->getValue($name).'" /></p>'
        );
    }

    public function submit(){
        return $this->surround('<button type ="submit">Envoyer</button>');
    }
}

//personal note. A question was raised on how to add labels for function l23
//public function input( $label, $type, $name){
    // echo '<p><label for="'.$name.'" />'.$label.'</label></p><p><input type="'.$type.'" id="" name="' .$name. '" id="'.$name.'" value="'.$this->getValue($name).'" /></p>';
// }
//and then add the following in index5.php
// echo $form->input('Nom :', 'text', 'username');
// echo $form->input('Email :','email','email');
// echo $form->input('Mot de passe :','password','password'); 
//
// Chat GP being asked on the wht is the difference between the two functions, here is the answer:
//     The two functions you provided are similar, but there is a key difference in how they handle the output.
//
// 1. The first function returns the HTML string representing the input element, allowing you to
// capture and use that HTML elsewhere in your code. It doesn't directly output anything to the browser.
//
// ```php
// public function input($name)
// {
//     return $this->surround('<input type="text" name="' . $name . '" value="' . $this->getValue($name) . '">');
// }
// ```
//
// You would use it like this:
//
// ```php
// $htmlInput = $yourObject->input('example');
// echo $htmlInput; // This is where you choose when and where to output the HTML
// ```
//
// 2. The second function uses `echo` to directly output the HTML string to the browser.
//
// ```php
// public function input($name)
// {
//     echo '<input type="text" name="' . $name . '" value="' . $this->getValue($name) . '">';
// }
// ```
//
// You would use it like this:
//
// ```php
// $yourObject->input('example'); // Outputs HTML directly to the browser
// ```
//
// So, the main difference is in how the HTML is handled: the first one returns the HTML 
// as a string, and the second one echoes it directly. The choice between them depends on whether
// you want to manipulate or use the HTML string in your code or if you want to output it 
//immediately.