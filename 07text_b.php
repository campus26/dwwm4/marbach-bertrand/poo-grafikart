<?php

class TextDeux
{
    private static $suffix = " €";
    
    public static function WithOrWithoutZero($number)
    {
        if ($number < 10) {
            return '0' . $number.self::$suffix;
        } else {
            return $number.self::$suffix;
        }
    }
}